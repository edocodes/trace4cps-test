
package codes.edo.trace4cps.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlChecker;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.VerificationSemantics;
import org.eclipse.trace4cps.analysis.mtl.causality.Cause;
import org.eclipse.trace4cps.analysis.mtl.causality.LtlCausalityChecker;
import org.eclipse.trace4cps.analysis.mtl.causality.LtlNnf;
import org.eclipse.trace4cps.analysis.mtl.causality.MtlNnf;
import org.eclipse.trace4cps.analysis.mtl.causality.RecursiveMemoizationCausalityChecker;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.google.common.collect.ImmutableSet;
import com.pholser.junit.quickcheck.From;
import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;

import codes.edo.trace4cps.test.gen.FormulaConfig;
import codes.edo.trace4cps.test.gen.MtlFormulaGen;
import codes.edo.trace4cps.test.gen.TestTrace;
import codes.edo.trace4cps.test.gen.TestTraceGen;

@RunWith(JUnitQuickcheck.class)
public class CausalityPropertyTest {
    private MtlChecker c;

    @BeforeClass
    public static void init() {
        // Log4j to stdout
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);
    }

    @Before
    public void setup() {
        c = new MtlChecker(1);
    }

    @Property(trials = 200000)
    public void FalsePropertyIffNonEmptyCause(@From(TestTraceGen.class) TestTrace trace,
            @From(MtlFormulaGen.class) @FormulaConfig(minLength = 1, maxLength = 5, timed = false, allowTrue = false,
                    allowNext = true, allowNestedNext = false) MtlFormula phi)
    {
        InformativePrefix val = c.check(trace.getTrace(), phi, null, VerificationSemantics.FourValued).informative();
        boolean finiteval = val == InformativePrefix.TRUE || val == InformativePrefix.STILL_TRUE;
        boolean weakval = val != InformativePrefix.FALSE;
        assumeTrue(weakval == false);
        LtlNnf nnf = LtlNnf.fromMtlFormula(phi, false);
        Set<String> causesweak = LtlCausalityChecker.computeCauses(trace.getTrace(), nnf, false, false).stream()
                .map(Object::toString).collect(Collectors.toSet());
        Set<String> causesfinite = LtlCausalityChecker.computeCauses(trace.getTrace(), nnf, false, true).stream()
                .map(Object::toString).collect(Collectors.toSet());
        System.out.format("%s\n%-11s - %s\n", phi, val, causesfinite);
        assertTrue("Weak cause for FALSE property should be non-empty", causesweak.size() > 0);

        // In the finite semantics, not every FALSE property has a cause (e.g. those with a next nested inside a
        // temporal operator, see page 76 of the thesis: Proposition 6.13 does not hold in the finite semantics)
        // assertTrue("Finite cause for FALSE property should be non-empty", causesfinite.size() > 0);
    }

    @Property(trials = 200000)
    public void TrueOrStillTruePropertyHasNoCause(@From(TestTraceGen.class) TestTrace trace,
            @From(MtlFormulaGen.class) @FormulaConfig(minLength = 1, maxLength = 10, timed = false, allowTrue = true,
                    allowNext = false) MtlFormula phi)
    {
        InformativePrefix val = c.check(trace.getTrace(), phi, null, VerificationSemantics.FourValued).informative();
        boolean finiteval = val == InformativePrefix.TRUE || val == InformativePrefix.STILL_TRUE;
        assumeTrue(finiteval == true);
        LtlNnf nnf = LtlNnf.fromMtlFormula(phi, false);
        Set<String> causesweak = LtlCausalityChecker.computeCauses(trace.getTrace(), nnf, false, false).stream()
                .map(Object::toString).collect(Collectors.toSet());
        Set<String> causesfinite = LtlCausalityChecker.computeCauses(trace.getTrace(), nnf, false, true).stream()
                .map(Object::toString).collect(Collectors.toSet());
        assertTrue("Cause for (STILL_)TRUE property should be empty",
                causesweak.size() == 0 && causesfinite.size() == 0);
    }

    // This property does not hold
//    @Property(trials = 200000)
//    public void FiniteCauseSupersetOfWeakCause(@From(TestTraceGen.class) TestTrace trace,
//            @From(MtlFormulaGen.class) @FormulaConfig(minLength = 1, maxLength = 10, timed = false, allowTrue = true,
//                    allowNext = true, allowNestedNext = false) MtlFormula phi)
//    {
//        assumeTrue(!MtlUtil.getSubformulas(phi).stream().anyMatch(f -> f instanceof MTLand));
//        InformativePrefix val = c.check(trace.getTrace(), phi, null, VerificationSemantics.FourValued).informative();
//        boolean finiteval = val == InformativePrefix.TRUE || val == InformativePrefix.STILL_TRUE;
//        boolean weakval = val != InformativePrefix.FALSE;
//        LtlNnf nnf = LtlNnf.fromMtlFormula(phi, false);
//        Set<String> causesweak = LtlCausalityChecker.computeCauses(trace.getTrace(), nnf, false, false).stream()
//                .map(Object::toString).collect(Collectors.toSet());
//        Set<String> causesfinite = LtlCausalityChecker.computeCauses(trace.getTrace(), nnf, false, true).stream()
//                .map(Object::toString).collect(Collectors.toSet());
//        if (!causesweak.equals(causesfinite)) {
//            System.out.format("Weak and finite causes do not agree - %s\n%s\n%s\nweak   %-5s %s\nfinite %-5s %s\n\n",
//                    val, phi, trace, weakval, causesweak, finiteval, causesfinite);
//        }
//        assertTrue("Weak cause set is subset of finite cause set", causesfinite.containsAll(causesweak));
//    }

    @Property(trials = 200000)
    public void SameVerdict(@From(TestTraceGen.class) TestTrace trace,
            @From(MtlFormulaGen.class) @FormulaConfig(minLength = 1, maxLength = 10, timed = false, allowTrue = true,
                    allowNext = true) MtlFormula phi)
    {
        boolean val1 = c.check(trace.getTrace(), phi, null, VerificationSemantics.Finite)
                .informative() == InformativePrefix.TRUE;
        LtlNnf nnf = LtlNnf.fromMtlFormula(phi, true);
        boolean val2 = LtlCausalityChecker.checkLTL(trace.getTrace(), nnf, false, true);
        boolean val3 = LtlCausalityChecker.checkLTL(trace.getTrace(), nnf, true, true);
        System.out.format("%s\n%s - %s - %s\n", phi, val1, val2, val3);
        assertEquals(val1, val2);
        assertEquals(val1, val3);
    }

    @Property(trials = 200000)
    public void UG_equals_UR_and_NewUSame(@From(TestTraceGen.class) TestTrace trace,
            @From(MtlFormulaGen.class) @FormulaConfig(minLength = 1, maxLength = 10, timed = false, allowTrue = true,
                    allowNext = true) MtlFormula phi)
    {
        LtlNnf nnfG = LtlNnf.fromMtlFormula(phi, true, false);
        LtlNnf nnfR = LtlNnf.fromMtlFormula(phi, true, true);

        Set<String> causes1 = LtlCausalityChecker.computeCauses(trace.getTrace(), nnfG, false, false).stream()
                .map(c -> c.toString()).collect(Collectors.toSet());
        Set<String> causes2 = LtlCausalityChecker.computeCauses(trace.getTrace(), nnfR, false, false).stream()
                .map(c -> c.toString()).collect(Collectors.toSet());
        Set<String> causes3 = LtlCausalityChecker.computeCauses(trace.getTrace(), nnfG, true, false).stream()
                .map(c -> c.toString()).collect(Collectors.toSet());
        Set<String> causes4 = LtlCausalityChecker.computeCauses(trace.getTrace(), nnfR, true, false).stream()
                .map(c -> c.toString()).collect(Collectors.toSet());

        System.out.println(phi);
        System.out.format("  %s\n", causes1);
        System.out.format("  %s\n", causes2);
        System.out.format("  %s\n", causes3);
        System.out.format("  %s\n", causes4);
        assertEquals(causes1, causes2);
        assertEquals(causes1, causes3);
        assertEquals(causes1, causes4);
    }

    @Property(trials = 200000)
    public void LTLCheckerAndMTLCheckerSame(@From(TestTraceGen.class) TestTrace trace,
            @From(MtlFormulaGen.class) @FormulaConfig(minLength = 1, maxLength = 10, timed = false, allowTrue = true,
                    allowNext = true) MtlFormula phi)
    {
        LtlNnf nnfLtl = LtlNnf.fromMtlFormula(phi, false);
        Collection<Cause> causesLtl = LtlCausalityChecker.computeCauses(trace.getTrace(), nnfLtl, true, true);

        MtlFormula nnfMtl = MtlNnf.toNnf(phi, false);
        RecursiveMemoizationCausalityChecker c = new RecursiveMemoizationCausalityChecker();
        c.check(trace.getTrace(), nnfMtl, ImmutableSet.of(), VerificationSemantics.Finite);
        Collection<Cause> causesMtl = c.getCauses();

        System.out.println(phi);
        System.out.println(causesLtl);
        System.out.println(causesMtl + "\n");

        assertEquals(causesLtl.stream().map(Object::toString).collect(Collectors.toSet()),
                causesMtl.stream().map(Object::toString).collect(Collectors.toSet()));
    }

    private static boolean equalCauses(Collection<Cause> a, Collection<Cause> b) {
        return a.stream().map(Object::toString).collect(Collectors.toSet())
                .equals(b.stream().map(Object::toString).collect(Collectors.toSet()));
    }
}
