
package codes.edo.trace4cps.test;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.VerificationSemantics;
import org.eclipse.trace4cps.analysis.mtl.check.RecursiveMemoizationChecker;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.pholser.junit.quickcheck.From;
import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;

import codes.edo.trace4cps.test.gen.FormulaConfig;
import codes.edo.trace4cps.test.gen.MtlFormulaGen;
import codes.edo.trace4cps.test.gen.TestTrace;
import codes.edo.trace4cps.test.gen.TestTraceGen;

@RunWith(JUnitQuickcheck.class)
public class FourValuedPropertyTest {
    @BeforeClass
    public static void init() {
        // Log4j to stdout
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);
    }

    @Property(trials = 200000)
    public void FourEqualsThreePlusFinite(@From(TestTraceGen.class) TestTrace trace,
            @From(MtlFormulaGen.class) @FormulaConfig(minLength = 3, maxLength = 10, timed = false, allowNext = true,
                    allowTrue = true) MtlFormula phi)
    {
        testFour(trace, phi);
    }

    private void testFour(TestTrace trace, MtlFormula phi) {
        RecursiveMemoizationChecker c = new RecursiveMemoizationChecker();
        InformativePrefix finite = c.check(trace.getTrace(), phi, null, VerificationSemantics.Finite);
        InformativePrefix three = c.check(trace.getTrace(), phi, null, VerificationSemantics.ThreeValued);
        InformativePrefix four = c.check(trace.getTrace(), phi, null, VerificationSemantics.FourValued);

        System.out.format("%s\nf: %-5s  3: %-15s  4: %-15s\n", phi, finite, three, four);
        System.out.println(trace + "\n");

        if (three == InformativePrefix.NON_INFORMATIVE) {
            if (finite == InformativePrefix.TRUE) {
                assertEquals(InformativePrefix.STILL_TRUE, four);
            } else {
                assertEquals(InformativePrefix.STILL_FALSE, four);
            }
        } else {
            assertEquals(finite, three);
            assertEquals(finite, four);
        }
    }
}
