package codes.edo.trace4cps.test.gen;

import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition;

import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

/*
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

public class DefaultAtomicPropositionGen extends Generator<DefaultAtomicProposition> {
    private static String[] attVals = new String[] {"1", "2", "3"};

    public DefaultAtomicPropositionGen() {
        super(DefaultAtomicProposition.class);
    }

    @Override
    public DefaultAtomicProposition generate(SourceOfRandomness random, GenerationStatus status) {
        return new DefaultAtomicProposition("e", random.choose(attVals));
    }
}