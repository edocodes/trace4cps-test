
package codes.edo.trace4cps.test.gen;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.trace4cps.core.impl.Interval;

import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

/*
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

public class IntervalGen extends Generator<Interval> {
    static List<Double> times = Stream.of(0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 5.0, 6.0).collect(Collectors.toList());

    public IntervalGen() {
        super(Interval.class);
    }

    @Override
    public Interval generate(SourceOfRandomness random, GenerationStatus status) {
        boolean timed = status.valueOf(MtlFormulaGen.timedKey).get();
        if (timed && random.nextBoolean()) {
            double start = random.nextBoolean() ? randomTime(random) : 0.0;
            double end = start + randomTime(random);
            return new Interval(start, random.nextBoolean(), end, random.nextBoolean());
        } else {
            return Interval.trivial();
        }
    }

    private static double randomTime(SourceOfRandomness random) {
        return random.nextBoolean() ? random.choose(times) : random.nextDouble(0, 6.0);
    }
}
