
package codes.edo.trace4cps.test.gen;

import static com.pholser.junit.quickcheck.internal.Ranges.checkRange;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;

import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.GenerationStatus.Key;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.internal.Ranges;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

public class MtlFormulaGen extends Generator<MtlFormula> {
    static Key<Integer> lengthKey = new Key<>("length", Integer.class);

    static Key<Boolean> timedKey = new Key<>("timed", Boolean.class);

    static Key<Boolean> allowTrueKey = new Key<>("allowTrue", Boolean.class);

    static Key<Boolean> allowNextKey = new Key<>("allowNext", Boolean.class);

    static Key<Boolean> allowNestedKey = new Key<>("allowNested", Boolean.class);

    static Key<Boolean> allowTemporalKey = new Key<>("allowTemporal", Boolean.class);

    static Key<Boolean> allowNestedNextKey = new Key<>("allowNestedNext", Boolean.class);

    private FormulaConfig config;

    public MtlFormulaGen() {
        super(MtlFormula.class);
    }

    public void configure(FormulaConfig config) {
        if (config != null) {
            checkRange(Ranges.Type.INTEGRAL, config.minLength(), config.maxLength());
        }

        this.config = config;
    }

    @Override
    public MtlFormula generate(SourceOfRandomness random, GenerationStatus status) {
        int length = this.config == null ? status.size()
                : random.nextInt(this.config.minLength(), this.config.maxLength());
        status.setValue(lengthKey, length);
        status.setValue(timedKey, this.config == null ? true : this.config.timed());
        status.setValue(allowTrueKey, this.config == null ? true : this.config.allowTrue());
        status.setValue(allowNextKey, this.config == null ? true : this.config.allowNext());
        status.setValue(allowNestedKey, this.config == null ? true : this.config.allowNested());
        status.setValue(allowTemporalKey, this.config == null ? true : this.config.allowTemporal());
        status.setValue(allowNestedNextKey, this.config == null ? true : this.config.allowNestedNext());

        return gen().make(AbstractMTLformulaGen.class).generate(random, status);
    }

    @Override
    public boolean canRegisterAsType(Class<?> type) {
        return false;
    }
}
