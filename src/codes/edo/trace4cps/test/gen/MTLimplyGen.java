package codes.edo.trace4cps.test.gen;

import org.eclipse.trace4cps.analysis.mtl.impl.AbstractMTLformula;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLimply;

import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

/*
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

public class MTLimplyGen extends Generator<MTLimply> {
    public MTLimplyGen() {
        super(MTLimply.class);
    }

    @Override
    public MTLimply generate(SourceOfRandomness random, GenerationStatus status) {
        Generator<AbstractMTLformula> sub = gen().make(AbstractMTLformulaGen.class);
        return new MTLimply(sub.generate(random, status), sub.generate(random, status));
    }
}
