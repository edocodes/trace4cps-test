
package codes.edo.trace4cps.test.gen;

import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.impl.AbstractMTLformula;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLtrue;

import com.google.common.collect.Lists;
import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

/*
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

public class AbstractMTLformulaGen extends Generator<AbstractMTLformula> {
    public AbstractMTLformulaGen() {
        super(AbstractMTLformula.class);
    }

    @Override
    public AbstractMTLformula generate(SourceOfRandomness random, GenerationStatus status) {
        int length = status.valueOf(MtlFormulaGen.lengthKey).get();
        boolean allowTrue = status.valueOf(MtlFormulaGen.allowTrueKey).get();
        boolean allowNext = status.valueOf(MtlFormulaGen.allowNextKey).get();
        boolean allowTemporal = status.valueOf(MtlFormulaGen.allowTemporalKey).get();

        if (length <= 1) {
            if (allowTrue && random.nextBoolean()) {
                return new MTLtrue();
            } else {
                return gen().make(DefaultAtomicPropositionGen.class).generate(random, status);
            }
        } else {
            status.setValue(MtlFormulaGen.lengthKey, length - 1);

            List<Class<? extends Generator<? extends AbstractMTLformula>>> nodes = Lists.newArrayList(MTLnotGen.class,
                    MTLandGen.class, MTLorGen.class, DefaultAtomicPropositionGen.class);
            if (allowTemporal) {
                nodes.addAll(Lists.newArrayList(MTLuntilGen.class, MTLfinallyGen.class, MTLgloballyGen.class));
            }
            if (allowNext) {
                nodes.add(MTLnextGen.class);
            }

            return gen().make(random.choose(nodes)).generate(random, status);
        }
    }
}
