
package codes.edo.trace4cps.test.gen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.impl.Event;

import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

/*
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

public class TestTraceGen extends Generator<TestTrace> {
    static String[] attVals = new String[] {"1", "2"};

    public TestTraceGen() {
        super(TestTrace.class);
    }

    @Override
    public TestTrace generate(SourceOfRandomness random, GenerationStatus status) {
        List<IEvent> trace = new ArrayList<>();
        for (int i = 0; i < random.nextInt(1, 5); i++) {
            String val = random.choose(attVals);
            IEvent e = new Event(i);
            e.setAttribute("e", val);
            trace.add(e);
        }
        return new TestTrace(trace);
    }

    @Override
    public List<TestTrace> doShrink(SourceOfRandomness random, TestTrace larger) {
        if (larger.getTrace().size() <= 1) {
            return Collections.emptyList();
        } else {
            List<TestTrace> res = new ArrayList<>();
            for (int i = 0; i < larger.getTrace().size(); i++) {
                TestTrace smaller = larger.copy();
                smaller.getTrace().remove(i);
                res.add(smaller);
            }
            return res;
        }
    }
}
