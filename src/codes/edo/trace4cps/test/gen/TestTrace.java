
package codes.edo.trace4cps.test.gen;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.trace4cps.core.IEvent;

/*
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

public class TestTrace {
    List<IEvent> trace;

    public TestTrace(List<IEvent> trace) {
        this.trace = trace;
    }

    public List<IEvent> getTrace() {
        return trace;
    }

    @Override
    public String toString() {
        return trace.stream().map(e -> String.format("(%d, %s)", e.getTimestamp(), e.getAttributes()))
                .collect(Collectors.joining(", "));
    }

    public TestTrace copy() {
        return new TestTrace(new ArrayList<IEvent>(trace));
    }
}
