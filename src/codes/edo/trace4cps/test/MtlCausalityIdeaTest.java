
package codes.edo.trace4cps.test;

import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.AND;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.F;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.G;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.IMPLY;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.NOT;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.OR;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.U;
import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.trace4cps.analysis.mtl.AtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlChecker;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.VerificationSemantics;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.impl.Event;
import org.eclipse.trace4cps.core.impl.Interval;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.Lists;

public class MtlCausalityIdeaTest {
    private MtlChecker c;

    @BeforeClass
    public static void init() {
        // Log4j to stdout
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);
    }

    @Before
    public void setup() {
        c = new MtlChecker(1);
    }

    @Test
    public void example1() {
        List<IEvent> trace1 = Lists.newArrayList(//
                new Event(0, "a", "a"), //
                new Event(2, "b", "b"));

        AtomicProposition a = new DefaultAtomicProposition("a", "a");
        AtomicProposition b = new DefaultAtomicProposition("b", "b");
        MtlFormula phi1 = U(a, b, new Interval(0.0, 2.5));

        List<IEvent> trace2 = Lists.newArrayList(//
                new Event(0, "a", "a"), new StartEvent(0.5, 1, 1),
                new Event(2, "b", "b"), new EndEvent(2.5, 1, 1));

        MtlFormula phi2 = AND(U(OR(a, OR(new StartEventAP(), new EndEventAP())), b), //
                F(AND( //
                        new StartEventAP(1, 1), //
                        U(NOT(new EndEventAP(1, 1)), b))));

        InformativePrefix v1 = c.check(trace1, phi1, null, VerificationSemantics.Finite).informative();
        InformativePrefix v2 = c.check(trace2, phi2, null, VerificationSemantics.Finite).informative();
        assertEquals(InformativePrefix.TRUE, v1);
        assertEquals(v1, v2);

        List<IEvent> trace1bad = Lists.newArrayList(new Event(0, "a", "a"), new Event(1, "c", "c"),
                new Event(2, "b", "b"));
        List<IEvent> trace2bad = Lists.newArrayList(new Event(0, "a", "a"), new StartEvent(0.5, 1, 1),
                new Event(1, "c", "c"), new Event(2, "b", "b"), new EndEvent(2.5, 1, 1));

        InformativePrefix v1bad = c.check(trace1bad, phi1, null, VerificationSemantics.Finite).informative();
        InformativePrefix v2bad = c.check(trace2bad, phi2, null, VerificationSemantics.Finite).informative();

        assertEquals(InformativePrefix.FALSE, v1bad);
        assertEquals(v1bad, v2bad);
    }

    @Test
    public void example2() {
        List<IEvent> trace1 = Lists.newArrayList(new Event(0, "a", "a", "b", "b"), new Event(1, "a", "a", "b", "b"),
                new Event(2, "b", "b"));

        AtomicProposition a = new DefaultAtomicProposition("a", "a");
        AtomicProposition b = new DefaultAtomicProposition("b", "b");
        MtlFormula phi1 = G(AND(a, b), new Interval(0.0, 1.5));

        List<IEvent> trace2 = Lists.newArrayList(new StartEvent(0, 1, 1), new Event(0, "a", "a", "b", "b"),
                new Event(1, "a", "a", "b", "b"), new EndEvent(1.5, 1, 1), new Event(2, "b", "b"));

        MtlFormula phi2 = U( //
                OR( //
                        OR(AND(a, b), OR(new StartEventAP(), new EndEventAP())), F(new StartEventAP(1, 1))),
                new EndEventAP(1, 1));

        InformativePrefix v2 = c.check(trace2, phi2, null, VerificationSemantics.Finite).informative();
        InformativePrefix v1 = c.check(trace1, phi1, null, VerificationSemantics.Finite).informative();
        assertEquals(InformativePrefix.TRUE, v1);
        assertEquals(v1, v2);

        MtlFormula phi1bad = G(AND(a, b), new Interval(0.0, 2.5));
        List<IEvent> trace2bad = Lists.newArrayList(new StartEvent(0, 1, 1), new Event(0, "a", "a", "b", "b"),
                new Event(1, "a", "a", "b", "b"), new Event(2, "b", "b"), new EndEvent(2.5, 1, 1));

        InformativePrefix v1bad = c.check(trace1, phi1bad, null, VerificationSemantics.Finite).informative();
        InformativePrefix v2bad = c.check(trace2bad, phi2, null, VerificationSemantics.Finite).informative();

        assertEquals(InformativePrefix.FALSE, v1bad);
        assertEquals(v1bad, v2bad);
    }

    @Test
    public void example3() {
        List<IEvent> trace1 = Lists.newArrayList(new Event(0, "a", "a"), new Event(1, "a", "a"),
                new Event(2, "b", "b"));

        AtomicProposition a = new DefaultAtomicProposition("a", "a");
        AtomicProposition b = new DefaultAtomicProposition("b", "b");
        MtlFormula phi1bad = G(U(a, b, new Interval(0.5, 2.5)), new Interval(0, 4));

        List<IEvent> trace2bad = Lists.newArrayList( //
                new StartEvent(0, 1, 1), //
                new EventEvent(0, 1), //
                new Event(0, "a", "a"), //
                new StartEvent(0.5, 2, 1), //
                new EventEvent(1, 2), //
                new Event(1, "a", "a"), //
                new StartEvent(1.5, 2, 2), //
                new EventEvent(2, 3), //
                new Event(2, "b", "b"), //
                new StartEvent(2.5, 2, 3), //
                new EndEvent(2.5, 2, 1), //
                new EndEvent(3.5, 2, 2), //
                new EndEvent(4, 1, 1), //
                new EndEvent(4.5, 2, 3));
        MtlFormula o21 = AND( //
                OR(U( //
                        OR( //
                                OR(a, OR(OR(new StartEventAP(), new EndEventAP()), new EventEventAP())),
                                F(new EventEventAP(1))),
                        b), OR(OR(new StartEventAP(), new EndEventAP()), new EventEventAP())), //
                IMPLY(new StartEventAP(2, 1), U(NOT(new EndEventAP(2, 1)), b)));
        MtlFormula o22 = AND( //
                OR(U( //
                        OR( //
                                OR(a, OR(OR(new StartEventAP(), new EndEventAP()), new EventEventAP())),
                                F(new EventEventAP(2))),
                        b), OR(OR(new StartEventAP(), new EndEventAP()), new EventEventAP())), //
                IMPLY(new StartEventAP(2, 2), U(NOT(new EndEventAP(2, 2)), b)));
        MtlFormula o23 = AND( //
                OR(U( //
                        OR( //
                                OR(a, OR(OR(new StartEventAP(), new EndEventAP()), new EventEventAP())),
                                F(new EventEventAP(3))),
                        b), OR(OR(new StartEventAP(), new EndEventAP()), new EventEventAP())), //
                IMPLY(new StartEventAP(2, 3), U(NOT(new EndEventAP(2, 3)), b)));
        MtlFormula phi2 = U( //
                OR( //
                        AND(AND(o21, o22), o23), //
                        F(new StartEventAP(1, 1))), //
                new EndEventAP(1, 1));

        InformativePrefix v1 = c.check(trace1, phi1bad, null, VerificationSemantics.Finite).informative();
        InformativePrefix v2 = c.check(trace2bad, phi2, null, VerificationSemantics.Finite).informative();
        assertEquals(InformativePrefix.FALSE, v1);
        assertEquals(v1, v2);

        MtlFormula phi1good = G(U(a, b, new Interval(0, 2.5)), new Interval(0, 4));

        List<IEvent> trace2good = Lists.newArrayList( //
                new StartEvent(0, 1, 1), //
                new StartEvent(0, 2, 1), //
                new EventEvent(0, 1), //
                new Event(0, "a", "a"), //
                new StartEvent(1, 2, 2), //
                new EventEvent(1, 2), //
                new Event(1, "a", "a"), //
                new StartEvent(2, 2, 3), //
                new EventEvent(2, 3), //
                new Event(2, "b", "b"), //
                new EndEvent(2.5, 2, 1), //
                new EndEvent(3.5, 2, 2), //
                new EndEvent(4, 1, 1), //
                new EndEvent(4.5, 2, 3));

        InformativePrefix v1good = c.check(trace1, phi1good, null, VerificationSemantics.Finite).informative();
        InformativePrefix v2good = c.check(trace2good, phi2, null, VerificationSemantics.Finite).informative();

        assertEquals(InformativePrefix.TRUE, v1good);
        assertEquals(v1good, v2good);
    }

    class StartEventAP extends DefaultAtomicProposition {
        public StartEventAP(Integer formulaIndex, Integer eventIndex) {
            super("_start", "specific", //
                    "_start_formula", formulaIndex.toString(), //
                    "_start_event", eventIndex.toString());
        }

        public StartEventAP() {
            super("_start", "any");
        }
    }

    class StartEvent implements IEvent {
        Number timestamp;

        Integer formula;

        Integer event;

        public StartEvent(Number timestamp, Integer formula, Integer event) {
            this.timestamp = timestamp;
            this.formula = formula;
            this.event = event;
        }

        @Override
        public boolean satisfies(AtomicProposition p) {
            if (p instanceof StartEventAP) {
                if (p.getAttributeValue("_start").equals("any")) {
                    return true;
                } else {
                    return p.getAttributeValue("_start_formula").equals(this.formula.toString())
                            && p.getAttributeValue("_start_event").equals(this.event.toString());
                }
            } else {
                return false;
            }
        }

        @Override
        public String getAttributeValue(String key) {
            return null;
        }

        @Override
        public Map<String, String> getAttributes() {
            return null;
        }

        @Override
        public void setAttribute(String key, String value) {
            throw new IllegalStateException("Not supported");
        }

        @Override
        public void clearAttributes() {
            throw new IllegalStateException("Not supported");
        }

        @Override
        public Number getTimestamp() {
            return this.timestamp;
        }
    }

    class EndEventAP extends DefaultAtomicProposition {
        public EndEventAP(Integer formulaIndex, Integer eventIndex) {
            super("_end", "specific", //
                    "_end_formula", formulaIndex.toString(), //
                    "_end_event", eventIndex.toString());
        }

        public EndEventAP() {
            super("_end", "any");
        }
    }

    class EndEvent implements IEvent {
        Number timestamp;

        Integer formula;

        Integer event;

        public EndEvent(Number timestamp, Integer formula, Integer event) {
            this.timestamp = timestamp;
            this.formula = formula;
            this.event = event;
        }

        @Override
        public boolean satisfies(AtomicProposition p) {
            if (p instanceof EndEventAP) {
                if (p.getAttributeValue("_end").equals("any")) {
                    return true;
                } else {
                    return p.getAttributeValue("_end_formula").equals(this.formula.toString())
                            && p.getAttributeValue("_end_event").equals(this.event.toString());
                }
            } else {
                return false;
            }
        }

        @Override
        public String getAttributeValue(String key) {
            return null;
        }

        @Override
        public Map<String, String> getAttributes() {
            return null;
        }

        @Override
        public void setAttribute(String key, String value) {
            throw new IllegalStateException("Not supported");
        }

        @Override
        public void clearAttributes() {
            throw new IllegalStateException("Not supported");
        }

        @Override
        public Number getTimestamp() {
            return this.timestamp;
        }
    }

    class EventEventAP extends DefaultAtomicProposition {
        public EventEventAP(Integer eventIndex) {
            super("_event", "specific", "_event_event", eventIndex.toString());
        }

        public EventEventAP() {
            super("_event", "any");
        }
    }

    class EventEvent implements IEvent {
        Number timestamp;

        Integer event;

        public EventEvent(Number timestamp, Integer event) {
            this.timestamp = timestamp;
            this.event = event;
        }

        @Override
        public boolean satisfies(AtomicProposition p) {
            if (p instanceof EventEventAP) {
                if (p.getAttributeValue("_event").equals("any")) {
                    return true;
                } else {
                    return p.getAttributeValue("_event_event").equals(this.event.toString());
                }
            } else {
                return false;
            }
        }

        @Override
        public String getAttributeValue(String key) {
            return null;
        }

        @Override
        public Map<String, String> getAttributes() {
            return null;
        }

        @Override
        public void setAttribute(String key, String value) {
            throw new IllegalStateException("Not supported");
        }

        @Override
        public void clearAttributes() {
            throw new IllegalStateException("Not supported");
        }

        @Override
        public Number getTimestamp() {
            return this.timestamp;
        }
    }
}
