/*
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package codes.edo.trace4cps.test

import codes.edo.trace4cps.test.gen.FormulaConfig
import codes.edo.trace4cps.test.gen.MtlFormulaGen
import codes.edo.trace4cps.test.gen.TestTrace
import codes.edo.trace4cps.test.gen.TestTraceGen
import com.google.common.collect.ImmutableList
import com.google.common.collect.ImmutableSet
import com.pholser.junit.quickcheck.From
import com.pholser.junit.quickcheck.Property
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck
import java.util.List
import java.util.stream.Stream
import org.apache.log4j.BasicConfigurator
import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition
import org.eclipse.trace4cps.analysis.mtl.MtlFormula
import org.eclipse.trace4cps.analysis.mtl.VerificationSemantics
import org.eclipse.trace4cps.analysis.mtl.causality.MtlNnf
import org.eclipse.trace4cps.analysis.mtl.causality.RecursiveMemoizationCausalityChecker
import org.eclipse.trace4cps.analysis.mtl.check.RecursiveMemoizationChecker
import org.eclipse.trace4cps.core.IEvent
import org.eclipse.trace4cps.core.impl.Event
import org.eclipse.trace4cps.core.impl.Interval
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith

import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.*
import static org.junit.Assert.assertEquals

@RunWith(typeof(JUnitQuickcheck))
class RecursiveMemoizationCausalityCheckerTest {
    @BeforeClass
    def static void init() {
        // Log4j to stdout
        BasicConfigurator.configure()
        Logger.rootLogger.setLevel(Level.INFO)
    }

    @Property(trials=200000)
    def generated(@From(typeof(TestTraceGen)) TestTrace trace,
        @From(typeof(MtlFormulaGen)) @FormulaConfig(minLength=1, maxLength=10, timed=true, allowNext=true) MtlFormula phi) {
        RMCCEqualsRMC(trace.trace, phi)
    }

    @Test def simple() {
        val List<IEvent> trace = ImmutableList.of(
            new Event(0, "e", "1"),
            new Event(1, "e", "2")
        )

        val e1 = new DefaultAtomicProposition("e", "1")
        val e2 = new DefaultAtomicProposition("e", "2")
        val e3 = new DefaultAtomicProposition("e", "3")

        Stream.of(
            G(e1),
            G(OR(e1, e2)),
            U(e1, e2),
            U(OR(e1, e2), e3),
            F(e2),
            F(e3),
            X(e2),
            X(e1),
            X(e3),
            G(e2, new Interval(0.2, 0.6)),
            G(e1, new Interval(0.2, 0.6)),
            G(e2, new Interval(0.2, 0.2)),
            G(e1, new Interval(0.2, 0.2)),
            G(e2, new Interval(0.2, true, 0.2, true)),
            G(e1, new Interval(0.2, true, 0.2, true)),
            G(e2, new Interval(0, 0.2)),
            G(e1, new Interval(0, 0.2)),
            G(e2, new Interval(0.2, false, 1, false)),
            G(e1, new Interval(0.2, false, 1, false)),
            G(e2, new Interval(0.2, false, 1, true)),
            G(e1, new Interval(0.2, false, 1, true)),
            G(e2, new Interval(0.2, false, 1.2, false)),
            G(e1, new Interval(0.2, false, 1.2, false)),
            G(e2, new Interval(0.2, false, 1.2, true)),
            G(e1, new Interval(0.2, false, 1.2, true)),
            NOT(U(e1, e2, new Interval(0.2, 0.6))),
            NOT(U(e1, e2, new Interval(0.2, 1.6)))
        ).forEach[phi|RMCCEqualsRMC(trace, phi)]
    }

    def RMCCEqualsRMC(List<IEvent> trace, MtlFormula phi) {
        val c1 = new RecursiveMemoizationChecker
        val r1 = c1.check(trace, phi, ImmutableSet.of(), VerificationSemantics.Finite)

        val nnf = MtlNnf.toNnf(phi, false);
        val c2 = new RecursiveMemoizationCausalityChecker
        val r2 = c2.check(trace, nnf, ImmutableSet.of(), VerificationSemantics.Finite)

        System.out.format("%s\nRMC: %5s     RMCC: %5s\n\n", phi, r1, r2)

        assertEquals(r1, r2)
    }
}
